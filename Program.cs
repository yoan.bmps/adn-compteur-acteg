﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

/*
* Projet =  ADN
* Détail = calcule du nombre d'occurence des bases ATGC
* Auteur = Yoan Bompas
* Date = 14.10.22
* Version = 1.0 
*/

namespace Atelier_Vendredi
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Exercice1_ABN();
            Console.ReadKey();
        }

        static void Exercice1_ABN()
        {
            StreamReader doc;
            doc = new StreamReader("G:\\Mon Drive\\Cfpt\\2022-2023\\5-Vendredi\\Atelier_du_devoloppeur\\Visual_Studio\\chromosome-11-partial.txt");
            String line;
            line = doc.ReadLine();

            int compteurA = 0;
            int compteurT = 0;
            int compteurG = 0;
            int compteurC = 0;


            while (line != null)
            {

                for (int i = 0; i < line.Length; i = i + 1)
                {

                    if (line[i] == 'A')
                    {
                        compteurA = compteurA + 1;
                    }

                    if (line[i] == 'T')
                    {
                        compteurT = compteurT + 1;
                    }

                    if (line[i] == 'G')
                    {
                        compteurG = compteurG + 1;
                    }

                    if (line[i] == 'C')
                    {
                        compteurC = compteurC + 1;
                    }
                }
                line = doc.ReadLine();
            }
            Console.WriteLine("Nombres de A = " + compteurA);
            Console.WriteLine("Nombres de T = " + compteurT);
            Console.WriteLine("Nombres de G = " + compteurG);
            Console.WriteLine("Nombres de C = " + compteurC);

        }

    }
}
